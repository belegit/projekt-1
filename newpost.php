<!DOCTYPE html>
<html lang="en">
<head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css"> <!-- Edit Post -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script> <!-- Markdown-Editor -->
  		<meta charset="UTF-8">
		<?php $xml = simplexml_load_file ("019_waf.xml") or die ("Cannot create Objekt"); ?> <!-- Laden von XML -->
		<title>Neuer Post</title>
</head>
<body>
<!----------------------- Dropdown Bar ---------------------------------------------------------------------------------------------->
<<iframe name="formDest" style="width:0;height:0;border:0; border:none;"></iframe>

<header class="container-fluid">
        <div class="row border-bottom">
            <div class="col-lg-12 ">
                <div class="btn-group" >
                    <button class="btn btn-primary" formaction="index.html">Home</button>
                    <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> Administrator</button>
                    <ul class="dropdown-menu" role="menu" data-hover="dropdown-menu">
                        <a href="#">New or Edit Post</a>
                        <a href="Upload.html">Upload Pictures</a>
                    </ul>
                    </div>       
                </div>    
            </div>
        </div>
</header>


<!-- Editieren eines Beitrages unter der Bedingung dass "Edit" Button gedr�ckt wurde und dann "save" gedr�ckt wurde --------------------------------->
<?php
if (isset($_POST["save"])){
$titelEdit = $_POST["titel"];
$inhaltEdit = $_POST["textarea"];
$id = $_POST["save"];

 $dom = new DOMDocument();
 $dom->load('019_waf.xml');
  $root = $dom->documentElement;
   $dom->getElementsByTagName("Titel")->item($id)->nodeValue = "";
    $dom->getElementsByTagName("Titel")->item($id)->appendChild($dom->createTextNode($titelEdit));
	$dom->getElementsByTagName("Inhalt")->item($id)->nodeValue = "";
	$dom->getElementsByTagName("Inhalt")->item($id)->appendChild($dom->createTextNode($inhaltEdit));
	

	    $dom->save('019_waf.xml');

}
?>



<!-------------------------------------------- Form mit Titel, inhalt , und save Button ---------------------------------------------------->
	<div class ="container" >
	<form action="savepost.php" method="POST" autocomplete="off" target="formDest">
		<div class="form-group" id="form">
			<label for="Titel">Titel</label> </br>
				<input name ="titel" id="dcm" type="text" class="form-control" size="215" maxlength="50" placeholder="Dein Titel...">
				<textarea id="inhalt"></textarea>
				<button type="submit" class ="btn">Save</button>
		</div>
  </form>

  </div>
  <hr>
 <!-- --------------------------------------------Liste aller Eintr�ge -------------------------------------------------------------------------------------------> 
  
 <div class="container">
  <h2>Alle Eintr�ge</h2>
  <table class="table table-bordered" id="Tai">

    <thead>
      <tr>
        <th style = "width: 40%">Titel</th>
        <th style = "width :40%">Datum</th>
        <th style ="width :5%"></th>
		<th style ="width :5%"></th>
	  </tr>
    </thead>
    <tbody>
		<?php $i = 0; foreach ($xml -> children() as $abc): ?>
				<tr>
					<td><?php  echo $abc -> Titel ;?></td>
					<td> <?php echo $abc -> Datum ;?></td>
					<form action ="EditSave.php" method = "post">
					<td><?php echo "<button type='submit' name='Edit' value='$i' id='Edit[$i]'> Edit ";?></td>
					</form>
					<form action="delete.php" method="post" autocomplete="off" target="formDest" onsubmit="return confirm('Bist du sicher dass du diesen Beitrag l�schen willst?');">
					<td><?php {echo "<button type='submit' name='Delete' value='$i' id='Delete[$i]'>Delete ";}?></td>
					</form>
					<?php $i++;?>
					<?php endforeach; ?>
				<tr>
		</tbody>
  </table>
</div>'
	<h1 id="demo"> </h1> 
<!----------------------------------------- Style vom Text - Editor ----------------------------------------------------------->	
	<style type="text/css">
	form {
	max-width: 1200px;
	margin : 2rem auto;
	border : 2px solid lightgrey;
	padding : 4rem;
	border-radius : 25px;
	background-color: rgba(0,0,0,0.1);
	}
	</style>
	



<script>var simplemde = new SimpleMDE({ element: document.getElementById("inhalt") });</script> <!-- Ausf�hren in eine Textarea -->
    
</body>
</html>

